import subprocess
from ast import literal_eval

program = "build/example_protein_representation.exe"
pdb_list  = open("data/pdb/pdb_list_only_proteins.txt", "r")
bad_pdb_list = open("bad_pdb_list.txt","w")
bad_pdb_list_error = open("bad_pdb_list_error.txt","w")
pdb_good_protein_list = open("good_pdb_list.txt", "w")
bad = 0
total = 0

for pdb in pdb_list:
	flag = True
	try:
		print("Processing: " + pdb)
		total = total + 1
		proc = subprocess.check_output([program, "data/pdb/"+pdb.strip()], stderr=subprocess.STDOUT, timeout=10)
	except subprocess.CalledProcessError as e:
		flag = False
		bad = bad+1
		string = str(e.output).replace("\\n","\n")
		bad_pdb_list.write(pdb);
		bad_pdb_list_error.write("PDB name: "+pdb)
		bad_pdb_list_error.write("Program error "+string+"\n \n")
	except subprocess.TimeoutExpired as te:
		flag = False
		bad = bad+1
		bad_pdb_list.write(pdb);
		bad_pdb_list_error.write("PDB name: "+pdb)
		bad_pdb_list_error.write("Program timeout error.\n \n")
	if flag:
		pdb_good_protein_list.write(pdb)

