import subprocess
program = "build/example_protein_representation.exe"
pdb_list  = open("data/pdb/pdb_list.txt", "r")
bad_pdb_list = open("bad_pdb_list.txt","w")
bad_pdb_list_error = open("bad_pdb_list_error.txt","w")
bad = 0
total = 0
for pdb in pdb_list:
	total = total + 1
	proc = subprocess.Popen([program, "data/pdb/"+pdb.strip()], bufsize=0, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = proc.communicate()
	if proc.returncode != 0:
		bad = bad+1	
		bad_pdb_list.write(pdb);
		bad_pdb_list_error.write("PDB name: "+pdb)
		bad_pdb_list_error.write("Program output "+stdout)
		bad_pdb_list_error.write("Program error "+stderr+"\n \n")
		

